/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation.redback.filters;

import au.net.abc.innovation.kinesis.snowplow.SnowplowEventModel;
import com.amazonaws.services.kinesis.connectors.interfaces.IFilter;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * 
 * @author Sam Mason (sam.mason@abc.net.au)
 */
public class ApplicationFilter implements IFilter<SnowplowEventModel>{
    
    private static final Log LOG = LogFactory.getLog(ApplicationFilter.class);
    
    private final List<String> exclusionList;
    
    public ApplicationFilter(List<String> exclusionList){
        this.exclusionList = exclusionList;
    }
    
    @Override
    public boolean keepRecord(SnowplowEventModel record){
        if(exclusionList != null){
            String appId = record.getApp_id();
            if(exclusionList.contains(appId)){
                LOG.info("Record dropped - matched appId exclusion filter " + appId);
                return false;
            }
        }
        return true;
    }    
}
