/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation.kinesis.snowplow;

import java.io.IOException;

/**
 * Builds an event model compatible with the current Redshift 0.9.6 schema
 * from the (version?) Kinesis stream record format (0.9.6 Redshift Schema) see:
 * 
 * https://github.com/snowplow/snowplow/blob/0.9.6/4-storage/redshift-storage/sql/atomic-def.sql
 * 
 * @author Sam Mason (sam.mason@abc.net.au)
 */
public class SnowplowStreamHandlerv0_9_6 extends BaseSnowplowStreamHandler{
    
    private static final int IDX_APP_ID                         = 0;
    private static final int IDX_PLATFORM                       = 1;
    private static final int IDX_ETL_TSTAMP                     = 2;
    private static final int IDX_COLLECTOR_TSTAMP               = 3;
    private static final int IDX_DEVICE_TSTAMP                  = 4;
    private static final int IDX_EVENT                          = 5;
    private static final int IDX_EVENT_ID                       = 6;
    private static final int IDX_TX_ID                          = 7;
    
    private static final int IDX_NAME_TRACKER                   = 8;
    private static final int IDX_V_TRACKER                      = 9;
    private static final int IDX_V_COLLECTOR                    = 10;
    private static final int IDX_V_ETL                          = 11;
    
    private static final int IDX_USER_ID                        = 12;
    private static final int IDX_USER_IP_ADDRESS                = 13;
    private static final int IDX_USER_FINGERPRINT               = 14;
    private static final int IDX_DOMAIN_USER_ID                 = 15;
    private static final int IDX_DOMAIN_SESSION_IDX             = 16;
    private static final int IDX_NETWORK_USER_ID                = 17;
    
    private static final int IDX_GEO_COUNTRY                    = 18;
    private static final int IDX_GEO_REGION                     = 19;
    private static final int IDX_GEO_CITY                       = 20;
    private static final int IDX_GEO_ZIPCODE                    = 21;
    private static final int IDX_GEO_LATITUDE                   = 22;
    private static final int IDX_GEO_LONGITUDE                  = 23;
    private static final int IDX_GEO_REGION_NAME                = 24;
    
    private static final int IDX_IP_ISP                         = 25;
    private static final int IDX_IP_ORGANIZATION                = 26;
    private static final int IDX_IP_DOMAIN                      = 27;
    private static final int IDX_IP_NETSPEED                    = 28;
    
    private static final int IDX_PAGE_URL                       = 29;
    private static final int IDX_PAGE_TITLE                     = 30;
    private static final int IDX_PAGE_REFERRER                  = 31;
        
    private static final int IDX_PAGE_URL_SCHEME                = 32;
    private static final int IDX_PAGE_URL_HOST                  = 33;
    private static final int IDX_PAGE_URL_PORT                  = 34;
    private static final int IDX_PAGE_URL_PATH                  = 35;
    private static final int IDX_PAGE_URL_QUERY                 = 36;
    private static final int IDX_PAGE_URL_FRAGMENT              = 37;
    
    private static final int IDX_REFR_URL_SCHEME                = 38;
    private static final int IDX_REFR_URL_HOST                  = 39;
    private static final int IDX_REFR_URL_PORT                  = 40;
    private static final int IDX_REFR_URL_PATH                  = 41;
    private static final int IDX_REFR_URL_QUERY                 = 42;
    private static final int IDX_REFR_URL_FRAGMENT              = 43;
    private static final int IDX_REFR_MEDIUM                    = 44;
    private static final int IDX_REFR_SOURCE                    = 45;
    private static final int IDX_REFR_TERM                      = 46;
    
    private static final int IDX_MKT_MEDIUM                     = 47;
    private static final int IDX_MKT_SOURCE                     = 48;
    private static final int IDX_MKT_TERM                       = 49;
    private static final int IDX_MKT_CONTENT                    = 50;
    private static final int IDX_MKT_CAMPAIGN                   = 51;
    
    private static final int IDX_CUSTOM_CONTEXTS                = 52;
    
    private static final int IDX_SE_CATEGORY                    = 53;
    private static final int IDX_SE_ACTION                      = 54;
    private static final int IDX_SE_LABEL                       = 55;
    private static final int IDX_SE_PROPERTY                    = 56;
    private static final int IDX_SE_VALUE                       = 57;
    
    private static final int IDX_UNSTRUCT_EVENT                 = 58;
    
    private static final int IDX_TR_ORDER_ID                    = 59;
    private static final int IDX_TR_AFFILIATION                 = 60;
    private static final int IDX_TR_TOTAL                       = 61;
    private static final int IDX_TR_TAX                         = 62;
    private static final int IDX_TR_SHIPPING                    = 63;
    private static final int IDX_TR_CITY                        = 64;
    private static final int IDX_TR_STATE                       = 65;
    private static final int IDX_TR_COUNTRY                     = 66;
    
    private static final int IDX_TI_ORDER_ID                    = 67;
    private static final int IDX_TI_SKU                         = 68;
    private static final int IDX_TI_NAME                        = 69;
    private static final int IDX_TI_CATEGORY                    = 70;
    private static final int IDX_TI_PRICE                       = 71;
    private static final int IDX_TI_QUANTITY                    = 72;
    
    private static final int IDX_PP_XOFFSET_MIN                 = 73;
    private static final int IDX_PP_XOFFSET_MAX                 = 74;
    private static final int IDX_PP_YOFFSET_MIN                 = 75;
    private static final int IDX_PP_YOFFSET_MAX                 = 76;
    
    private static final int IDX_USER_AGENT                     = 77;
    
    private static final int IDX_BR_NAME                        = 78;
    private static final int IDX_BR_FAMILY                      = 79;
    private static final int IDX_BR_VERSION                     = 80;
    private static final int IDX_BR_TYPE                        = 81;
    private static final int IDX_BR_RENDER_ENGINE               = 82;
    private static final int IDX_BR_LANG                        = 83;
    
    private static final int IDX_BR_FEAT_PDF                    = 84;
    private static final int IDX_BR_FEAT_FLASH                  = 85;
    private static final int IDX_BR_FEAT_JAVA                   = 86;
    private static final int IDX_BR_FEAT_DIRECTOR               = 87;
    private static final int IDX_BR_FEAT_QUICKTIME              = 88;
    private static final int IDX_BR_FEAT_REALPLAYER             = 89;
    private static final int IDX_BR_FEAT_WINDOWSMEDIA           = 90;
    private static final int IDX_BR_FEAT_GEARS                  = 91;
    private static final int IDX_BR_FEAT_SILVERLIGHT            = 92;
    
    private static final int IDX_BR_COOKIES                     = 93;
    private static final int IDX_BR_COLOR_DEPTH                 = 94;
    private static final int IDX_BR_VIEW_WIDTH                  = 95;
    private static final int IDX_BR_VIEW_HEIGHT                 = 96;
    
    private static final int IDX_OS_NAME                        = 97;
    private static final int IDX_OS_FAMILY                      = 98;
    private static final int IDX_OS_MANUFACTURER                = 99;
    private static final int IDX_OS_TIMEZONE                    = 100;
    
    private static final int IDX_DEVICE_TYPE                    = 101;
    private static final int IDX_DEVICE_ISMOBILE                = 102;
    private static final int IDX_DEVICE_SCREEN_WIDTH            = 103;
    private static final int IDX_DEVICE_SCREEN_HEIGHT           = 104;
    
    private static final int IDX_DOC_CHARSET                    = 105;
    private static final int IDX_DOC_WIDTH                      = 106;
    private static final int IDX_DOC_HEIGHT                     = 107;
        
    @Override
    public SnowplowEventModel process(String record) throws IOException{
        SnowplowEventModel streamModel = new SnowplowEventModel();
        try{
            //added in case there are blank columns at the end.
            record+=" ";
            String[] eventColumns = record.split(DELIMITER, -1);
            
            streamModel.setApp_id(eventColumns[IDX_APP_ID]);
            streamModel.setPlatform(eventColumns[IDX_PLATFORM]);
            streamModel.setEtl_tstamp(parseDate(eventColumns[IDX_ETL_TSTAMP]));
            streamModel.setCollector_tstamp(parseDate(eventColumns[IDX_COLLECTOR_TSTAMP]));
            streamModel.setDvce_tstamp(parseDate(eventColumns[IDX_DEVICE_TSTAMP]));
            streamModel.setEvent(eventColumns[IDX_EVENT]);
            streamModel.setEvent_id(eventColumns[IDX_EVENT_ID]);
            streamModel.setTxn_id(parseInteger(eventColumns[IDX_TX_ID]));
                        
            streamModel.setName_tracker(eventColumns[IDX_NAME_TRACKER]);
            streamModel.setV_tracker(eventColumns[IDX_V_TRACKER]);
            streamModel.setV_collector(eventColumns[IDX_V_COLLECTOR]);
            streamModel.setV_etl(eventColumns[IDX_V_ETL]);
                        
            streamModel.setUser_id(eventColumns[IDX_USER_ID]);
            streamModel.setUser_ipaddress(eventColumns[IDX_USER_IP_ADDRESS]);
            streamModel.setUser_fingerprint(eventColumns[IDX_USER_FINGERPRINT]);
            streamModel.setDomain_userid(eventColumns[IDX_DOMAIN_USER_ID]);
            streamModel.setDomain_sessionidx(parseShort(eventColumns[IDX_DOMAIN_SESSION_IDX]));
            streamModel.setNetwork_userid(eventColumns[IDX_NETWORK_USER_ID]);
            
            streamModel.setGeo_country(eventColumns[IDX_GEO_COUNTRY]);
            streamModel.setGeo_region(eventColumns[IDX_GEO_REGION]);
            streamModel.setGeo_city(eventColumns[IDX_GEO_CITY]);
            streamModel.setGeo_zipcode(eventColumns[IDX_GEO_ZIPCODE]);
            streamModel.setGeo_latitude(parseDouble(eventColumns[IDX_GEO_LATITUDE]));
            streamModel.setGeo_longitude(parseDouble(eventColumns[IDX_GEO_LONGITUDE]));
            streamModel.setGeo_region_name(eventColumns[IDX_GEO_REGION_NAME]);
            
            streamModel.setIp_isp(eventColumns[IDX_IP_ISP]);
            streamModel.setIp_organization(eventColumns[IDX_IP_ORGANIZATION]);
            streamModel.setIp_domain(eventColumns[IDX_IP_DOMAIN]);
            streamModel.setIp_netspeed(eventColumns[IDX_IP_NETSPEED]);
            
            streamModel.setPage_url(eventColumns[IDX_PAGE_URL]);
            streamModel.setPage_title(eventColumns[IDX_PAGE_TITLE]);
            streamModel.setPage_referrer(eventColumns[IDX_PAGE_REFERRER]);
                        
            streamModel.setPage_urlscheme(eventColumns[IDX_PAGE_URL_SCHEME]);
            streamModel.setPage_urlhost(eventColumns[IDX_PAGE_URL_HOST]);
            streamModel.setPage_urlport(parseInteger(eventColumns[IDX_PAGE_URL_PORT]));
            streamModel.setPage_urlpath(eventColumns[IDX_PAGE_URL_PATH]);
            streamModel.setPage_urlquery(eventColumns[IDX_PAGE_URL_QUERY]);
            streamModel.setPage_urlfragment(eventColumns[IDX_PAGE_URL_FRAGMENT]);
            
            streamModel.setRefr_urlscheme(eventColumns[IDX_REFR_URL_SCHEME]);
            streamModel.setRefr_urlhost(eventColumns[IDX_REFR_URL_HOST]);
            streamModel.setRefr_urlport(parseInteger(eventColumns[IDX_REFR_URL_PORT]));
            streamModel.setRefr_urlpath(eventColumns[IDX_REFR_URL_PATH]);
            streamModel.setRefr_urlquery(eventColumns[IDX_REFR_URL_QUERY]);
            streamModel.setRefr_urlfragment(eventColumns[IDX_REFR_URL_FRAGMENT]);
            streamModel.setRefr_medium(eventColumns[IDX_REFR_MEDIUM]);
            streamModel.setRefr_source(eventColumns[IDX_REFR_SOURCE]);
            streamModel.setRefr_term(eventColumns[IDX_REFR_TERM]);
            
            streamModel.setMkt_medium(eventColumns[IDX_MKT_MEDIUM]);
            streamModel.setMkt_source(eventColumns[IDX_MKT_SOURCE]);
            streamModel.setMkt_term(eventColumns[IDX_MKT_TERM]);
            streamModel.setMkt_content(eventColumns[IDX_MKT_CONTENT]);
            streamModel.setMkt_campaign(eventColumns[IDX_MKT_CAMPAIGN]);
            
            streamModel.setContexts(eventColumns[IDX_CUSTOM_CONTEXTS]);
                                    
            streamModel.setSe_category(eventColumns[IDX_SE_CATEGORY]);
            streamModel.setSe_action(eventColumns[IDX_SE_ACTION]);
            streamModel.setSe_label(eventColumns[IDX_SE_LABEL]);
            streamModel.setSe_property(eventColumns[IDX_SE_PROPERTY]);
            streamModel.setSe_value(parseDouble(eventColumns[IDX_SE_VALUE]));
            
            streamModel.setUnstruct_event(eventColumns[IDX_UNSTRUCT_EVENT]);
            
            streamModel.setTr_orderid(eventColumns[IDX_TR_ORDER_ID]);
            streamModel.setTr_affiliation(eventColumns[IDX_TR_AFFILIATION]);
            streamModel.setTr_total(parseDecimal(eventColumns[IDX_TR_TOTAL]));
            streamModel.setTr_tax(parseDecimal(eventColumns[IDX_TR_TAX]));
            streamModel.setTr_shipping(parseDecimal(eventColumns[IDX_TR_SHIPPING]));
            streamModel.setTr_city(eventColumns[IDX_TR_CITY]);
            streamModel.setTr_state(eventColumns[IDX_TR_STATE]);
            streamModel.setTr_country(eventColumns[IDX_TR_COUNTRY]);
            
            streamModel.setTi_orderid(eventColumns[IDX_TI_ORDER_ID]);
            streamModel.setTi_sku(eventColumns[IDX_TI_SKU]);
            streamModel.setTi_name(eventColumns[IDX_TI_NAME]);
            streamModel.setTi_category(eventColumns[IDX_TI_CATEGORY]);
            streamModel.setTi_price(parseDecimal(eventColumns[IDX_TI_PRICE]));
            streamModel.setTi_quantity(parseInteger(eventColumns[IDX_TI_QUANTITY]));
    
            streamModel.setPp_xoffset_min(IDX_PP_XOFFSET_MIN);
            streamModel.setPp_xoffset_max(IDX_PP_XOFFSET_MAX);
            streamModel.setPp_yoffset_min(IDX_PP_YOFFSET_MIN);
            streamModel.setPp_yoffset_max(IDX_PP_YOFFSET_MAX);
            streamModel.setUseragent(eventColumns[IDX_USER_AGENT]);
                        
            streamModel.setBr_name(eventColumns[IDX_BR_NAME]);
            streamModel.setBr_family(eventColumns[IDX_BR_FAMILY]);
            streamModel.setBr_version(eventColumns[IDX_BR_VERSION]);
            streamModel.setBr_type(eventColumns[IDX_BR_TYPE]);
            streamModel.setBr_renderengine(eventColumns[IDX_BR_RENDER_ENGINE]);
            streamModel.setBr_lang(eventColumns[IDX_BR_LANG]);
            
            streamModel.setBr_features_pdf(parseBoolean(eventColumns[IDX_BR_FEAT_PDF]));
            streamModel.setBr_features_flash(parseBoolean(eventColumns[IDX_BR_FEAT_FLASH]));
            streamModel.setBr_features_java(parseBoolean(eventColumns[IDX_BR_FEAT_JAVA]));
            streamModel.setBr_features_director(parseBoolean(eventColumns[IDX_BR_FEAT_DIRECTOR]));
            streamModel.setBr_features_quicktime(parseBoolean(eventColumns[IDX_BR_FEAT_QUICKTIME]));
            streamModel.setBr_features_realplayer(parseBoolean(eventColumns[IDX_BR_FEAT_REALPLAYER]));
            streamModel.setBr_features_windowsmedia(parseBoolean(eventColumns[IDX_BR_FEAT_WINDOWSMEDIA]));
            streamModel.setBr_features_gears(parseBoolean(eventColumns[IDX_BR_FEAT_GEARS]));
            streamModel.setBr_features_silverlight(parseBoolean(eventColumns[IDX_BR_FEAT_SILVERLIGHT]));
            
            streamModel.setBr_cookies(parseBoolean(eventColumns[IDX_BR_COOKIES]));
            streamModel.setBr_colordepth(eventColumns[IDX_BR_COLOR_DEPTH]);
            streamModel.setBr_viewwidth(parseInteger(eventColumns[IDX_BR_VIEW_WIDTH]));
            streamModel.setBr_viewheight(parseInteger(eventColumns[IDX_BR_VIEW_HEIGHT]));
            
            streamModel.setOs_name(eventColumns[IDX_OS_NAME]);
            streamModel.setOs_family(eventColumns[IDX_OS_FAMILY]);
            streamModel.setOs_manufacturer(eventColumns[IDX_OS_MANUFACTURER]);
            streamModel.setOs_timezone(eventColumns[IDX_OS_TIMEZONE]);
            
            streamModel.setDvce_type(eventColumns[IDX_DEVICE_TYPE]);
            streamModel.setDvce_ismobile(parseBoolean(eventColumns[IDX_DEVICE_ISMOBILE]));
            streamModel.setDvce_screenwidth(parseInteger(eventColumns[IDX_DEVICE_SCREEN_WIDTH]));
            streamModel.setDvce_screenheight(parseInteger(eventColumns[IDX_DEVICE_SCREEN_HEIGHT]));
            
            streamModel.setDoc_charset(eventColumns[IDX_DOC_CHARSET]);
            streamModel.setDoc_width(parseInteger(eventColumns[IDX_DOC_WIDTH]));
            streamModel.setDoc_height(parseInteger(eventColumns[IDX_DOC_HEIGHT]));
            
        }
        catch(Exception e){
            System.out.println(record);
            throw new IOException(e);
        }
        return streamModel;
    }
}
