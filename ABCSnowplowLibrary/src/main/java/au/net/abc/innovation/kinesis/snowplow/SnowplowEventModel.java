/**
 * Copyright 2015 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation.kinesis.snowplow;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Snowplow 0.9.6 Event Schema Model
 * 
 * see : https://github.com/snowplow/snowplow/blob/0.9.0/4-storage/redshift-storage/sql/atomic-def.sql
 * @author Sam Mason (sam.mason@abc.net.au)
 */
public class SnowplowEventModel {
    
    private String app_id;
    private String platform;
    private Date etl_tstamp;
    private Date collector_tstamp;
    private Date dvce_tstamp;
    private String event;
    private String event_id;
    private Integer txn_id;
    private String name_tracker;
    
    private String v_tracker;
    private String v_collector;
    private String v_etl;
    
    private String user_id;
    private String user_ipaddress;
    private String user_fingerprint;
    private String domain_userid;
    private Short domain_sessionidx;
    private String network_userid;
    
    private String geo_country;
    private String geo_region;
    private String geo_city;
    private String geo_zipcode;
    private Double geo_latitude;
    private Double geo_longitude;
    private String geo_region_name;
    
    private String ip_isp;
    private String ip_organization;
    private String ip_domain;
    private String ip_netspeed;    
    
    private String page_url;
    private String page_title;
    private String page_referrer;
    
    private String page_urlscheme;
    private String page_urlhost;
    private Integer page_urlport;
    private String page_urlpath;
    private String page_urlquery;
    private String page_urlfragment;
    
    private String refr_urlscheme;
    private String refr_urlhost;
    private Integer refr_urlport;
    private String refr_urlpath;
    private String refr_urlquery;
    private String refr_urlfragment;
    private String refr_medium;
    private String refr_source;
    private String refr_term;
    
    private String mkt_medium;
    private String mkt_source;
    private String mkt_term;
    private String mkt_content;
    private String mkt_campaign;
    
    private String contexts;
    
    private String se_category;
    private String se_action;
    private String se_label;
    private String se_property;
    private Double se_value;
    
    private String unstruct_event;
    
    private String tr_orderid;
    private String tr_affiliation;
    private BigDecimal tr_total;
    private BigDecimal tr_tax;    
    private BigDecimal tr_shipping;
    private String tr_city;
    private String tr_state;
    private String tr_country;
    
    private String ti_orderid;
    private String ti_sku;
    private String ti_name;
    private String ti_category;
    private BigDecimal ti_price;
    private Integer ti_quantity;
    
    private Integer pp_xoffset_min;
    private Integer pp_xoffset_max;
    private Integer pp_yoffset_min;
    private Integer pp_yoffset_max;

    private String useragent;
    
    private String br_name;
    private String br_family;
    private String br_version;
    private String br_type;
    private String br_renderengine;
    private String br_lang;
    
    private Boolean br_features_pdf;
    private Boolean br_features_flash;
    private Boolean br_features_java;
    private Boolean br_features_director;
    private Boolean br_features_quicktime;
    private Boolean br_features_realplayer;
    private Boolean br_features_windowsmedia;
    private Boolean br_features_gears;
    private Boolean br_features_silverlight;
    
    private Boolean br_cookies;
    
    private String br_colordepth;
    private Integer br_viewwidth;
    private Integer br_viewheight;
 
    private String os_name;
    private String os_family;
    private String os_manufacturer;
    private String os_timezone;
    
    private String dvce_type;
    private Boolean dvce_ismobile;
    private Integer dvce_screenwidth;
    private Integer dvce_screenheight;
    
    private String doc_charset;
    private Integer doc_width;
    private Integer doc_height;
    
    private String tr_currency;
    private BigDecimal tr_total_base;
    private BigDecimal tr_tax_base;
    private BigDecimal tr_shipping_base;
    private String ti_currency;
    private BigDecimal ti_price_base;
    private String base_currency;
    
    private String geo_timezone;
    
    private String mkt_clickId;
    private String mkt_network;
    
    private String etl_tags;
    
    private Date dvce_sent_tstamp;
    
    private String refr_domain_userId;
    private Date refr_dvce_tstamp;
    
    private String derived_contexts;
    
    private String domain_sessionId;
    
    private Date derived_tstamp;
        
    public SnowplowEventModel(){}

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Date getEtl_tstamp() {
        return etl_tstamp;
    }

    public void setEtl_tstamp(Date etl_tstamp) {
        this.etl_tstamp = etl_tstamp;
    }

    public Date getCollector_tstamp() {
        return collector_tstamp;
    }

    public void setCollector_tstamp(Date collector_tstamp) {
        this.collector_tstamp = collector_tstamp;
    }

    public Date getDvce_tstamp() {
        return dvce_tstamp;
    }

    public void setDvce_tstamp(Date dvce_tstamp) {
        this.dvce_tstamp = dvce_tstamp;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public Integer getTxn_id() {
        return txn_id;
    }

    public void setTxn_id(Integer txn_id) {
        this.txn_id = txn_id;
    }

    public String getName_tracker() {
        return name_tracker;
    }

    public void setName_tracker(String name_tracker) {
        this.name_tracker = name_tracker;
    }

    public String getV_tracker() {
        return v_tracker;
    }

    public void setV_tracker(String v_tracker) {
        this.v_tracker = v_tracker;
    }

    public String getV_collector() {
        return v_collector;
    }

    public void setV_collector(String v_collector) {
        this.v_collector = v_collector;
    }

    public String getV_etl() {
        return v_etl;
    }

    public void setV_etl(String v_etl) {
        this.v_etl = v_etl;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_ipaddress() {
        return user_ipaddress;
    }

    public void setUser_ipaddress(String user_ipaddress) {
        this.user_ipaddress = user_ipaddress;
    }

    public String getUser_fingerprint() {
        return user_fingerprint;
    }

    public void setUser_fingerprint(String user_fingerprint) {
        this.user_fingerprint = user_fingerprint;
    }

    public String getDomain_userid() {
        return domain_userid;
    }

    public void setDomain_userid(String domain_userid) {
        this.domain_userid = domain_userid;
    }

    public Short getDomain_sessionidx() {
        return domain_sessionidx;
    }

    public void setDomain_sessionidx(Short domain_sessionidx) {
        this.domain_sessionidx = domain_sessionidx;
    }

    public String getNetwork_userid() {
        return network_userid;
    }

    public void setNetwork_userid(String network_userid) {
        this.network_userid = network_userid;
    }

    public String getGeo_country() {
        return geo_country;
    }

    public void setGeo_country(String geo_country) {
        this.geo_country = geo_country;
    }

    public String getGeo_region() {
        return geo_region;
    }

    public void setGeo_region(String geo_region) {
        this.geo_region = geo_region;
    }

    public String getGeo_city() {
        return geo_city;
    }

    public void setGeo_city(String geo_city) {
        this.geo_city = geo_city;
    }

    public String getGeo_zipcode() {
        return geo_zipcode;
    }

    public void setGeo_zipcode(String geo_zipcode) {
        this.geo_zipcode = geo_zipcode;
    }

    public Double getGeo_latitude() {
        return geo_latitude;
    }

    public void setGeo_latitude(Double geo_latitude) {
        this.geo_latitude = geo_latitude;
    }

    public Double getGeo_longitude() {
        return geo_longitude;
    }

    public void setGeo_longitude(Double geo_longitude) {
        this.geo_longitude = geo_longitude;
    }

    public String getGeo_region_name() {
        return geo_region_name;
    }

    public void setGeo_region_name(String geo_region_name) {
        this.geo_region_name = geo_region_name;
    }

    public String getIp_isp() {
        return ip_isp;
    }

    public void setIp_isp(String ip_isp) {
        this.ip_isp = ip_isp;
    }

    public String getIp_organization() {
        return ip_organization;
    }

    public void setIp_organization(String ip_organization) {
        this.ip_organization = ip_organization;
    }

    public String getIp_domain() {
        return ip_domain;
    }

    public void setIp_domain(String ip_domain) {
        this.ip_domain = ip_domain;
    }

    public String getIp_netspeed() {
        return ip_netspeed;
    }

    public void setIp_netspeed(String ip_netspeed) {
        this.ip_netspeed = ip_netspeed;
    }

    public String getPage_url() {
        return page_url;
    }

    public void setPage_url(String page_url) {
        this.page_url = page_url;
    }

    public String getPage_title() {
        return page_title;
    }

    public void setPage_title(String page_title) {
        this.page_title = page_title;
    }

    public String getPage_referrer() {
        return page_referrer;
    }

    public void setPage_referrer(String page_referrer) {
        this.page_referrer = page_referrer;
    }

    public String getPage_urlscheme() {
        return page_urlscheme;
    }

    public void setPage_urlscheme(String page_urlscheme) {
        this.page_urlscheme = page_urlscheme;
    }

    public String getPage_urlhost() {
        return page_urlhost;
    }

    public void setPage_urlhost(String page_urlhost) {
        this.page_urlhost = page_urlhost;
    }

    public Integer getPage_urlport() {
        return page_urlport;
    }

    public void setPage_urlport(Integer page_urlport) {
        this.page_urlport = page_urlport;
    }

    public String getPage_urlpath() {
        return page_urlpath;
    }

    public void setPage_urlpath(String page_urlpath) {
        this.page_urlpath = page_urlpath;
    }

    public String getPage_urlquery() {
        return page_urlquery;
    }

    public void setPage_urlquery(String page_urlquery) {
        this.page_urlquery = page_urlquery;
    }

    public String getPage_urlfragment() {
        return page_urlfragment;
    }

    public void setPage_urlfragment(String page_urlfragment) {
        this.page_urlfragment = page_urlfragment;
    }

    public String getRefr_urlscheme() {
        return refr_urlscheme;
    }

    public void setRefr_urlscheme(String refr_urlscheme) {
        this.refr_urlscheme = refr_urlscheme;
    }

    public String getRefr_urlhost() {
        return refr_urlhost;
    }

    public void setRefr_urlhost(String refr_urlhost) {
        this.refr_urlhost = refr_urlhost;
    }

    public Integer getRefr_urlport() {
        return refr_urlport;
    }

    public void setRefr_urlport(Integer refr_urlport) {
        this.refr_urlport = refr_urlport;
    }

    public String getRefr_urlpath() {
        return refr_urlpath;
    }

    public void setRefr_urlpath(String refr_urlpath) {
        this.refr_urlpath = refr_urlpath;
    }

    public String getRefr_urlquery() {
        return refr_urlquery;
    }

    public void setRefr_urlquery(String refr_urlquery) {
        this.refr_urlquery = refr_urlquery;
    }

    public String getRefr_urlfragment() {
        return refr_urlfragment;
    }

    public void setRefr_urlfragment(String refr_urlfragment) {
        this.refr_urlfragment = refr_urlfragment;
    }

    public String getRefr_medium() {
        return refr_medium;
    }

    public void setRefr_medium(String refr_medium) {
        this.refr_medium = refr_medium;
    }

    public String getRefr_source() {
        return refr_source;
    }

    public void setRefr_source(String refr_source) {
        this.refr_source = refr_source;
    }

    public String getRefr_term() {
        return refr_term;
    }

    public void setRefr_term(String refr_term) {
        this.refr_term = refr_term;
    }

    public String getMkt_medium() {
        return mkt_medium;
    }

    public void setMkt_medium(String mkt_medium) {
        this.mkt_medium = mkt_medium;
    }

    public String getMkt_source() {
        return mkt_source;
    }

    public void setMkt_source(String mkt_source) {
        this.mkt_source = mkt_source;
    }

    public String getMkt_term() {
        return mkt_term;
    }

    public void setMkt_term(String mkt_term) {
        this.mkt_term = mkt_term;
    }

    public String getMkt_content() {
        return mkt_content;
    }

    public void setMkt_content(String mkt_content) {
        this.mkt_content = mkt_content;
    }

    public String getMkt_campaign() {
        return mkt_campaign;
    }

    public void setMkt_campaign(String mkt_campaign) {
        this.mkt_campaign = mkt_campaign;
    }

    public String getContexts() {
        return contexts;
    }

    public void setContexts(String contexts) {
        this.contexts = contexts;
    }

    public String getSe_category() {
        return se_category;
    }

    public void setSe_category(String se_category) {
        this.se_category = se_category;
    }

    public String getSe_action() {
        return se_action;
    }

    public void setSe_action(String se_action) {
        this.se_action = se_action;
    }

    public String getSe_label() {
        return se_label;
    }

    public void setSe_label(String se_label) {
        this.se_label = se_label;
    }

    public String getSe_property() {
        return se_property;
    }

    public void setSe_property(String se_property) {
        this.se_property = se_property;
    }

    public Double getSe_value() {
        return se_value;
    }

    public void setSe_value(Double se_value) {
        this.se_value = se_value;
    }

    public String getUnstruct_event() {
        return unstruct_event;
    }

    public void setUnstruct_event(String unstruct_event) {
        this.unstruct_event = unstruct_event;
    }

    public String getTr_orderid() {
        return tr_orderid;
    }

    public void setTr_orderid(String tr_orderid) {
        this.tr_orderid = tr_orderid;
    }

    public String getTr_affiliation() {
        return tr_affiliation;
    }

    public void setTr_affiliation(String tr_affiliation) {
        this.tr_affiliation = tr_affiliation;
    }

    public BigDecimal getTr_total() {
        return tr_total;
    }

    public void setTr_total(BigDecimal tr_total) {
        this.tr_total = tr_total;
    }

    public BigDecimal getTr_tax() {
        return tr_tax;
    }

    public void setTr_tax(BigDecimal tr_tax) {
        this.tr_tax = tr_tax;
    }

    public BigDecimal getTr_shipping() {
        return tr_shipping;
    }

    public void setTr_shipping(BigDecimal tr_shipping) {
        this.tr_shipping = tr_shipping;
    }

    public String getTr_city() {
        return tr_city;
    }

    public void setTr_city(String tr_city) {
        this.tr_city = tr_city;
    }

    public String getTr_state() {
        return tr_state;
    }

    public void setTr_state(String tr_state) {
        this.tr_state = tr_state;
    }

    public String getTr_country() {
        return tr_country;
    }

    public void setTr_country(String tr_country) {
        this.tr_country = tr_country;
    }

    public String getTi_orderid() {
        return ti_orderid;
    }

    public void setTi_orderid(String ti_orderid) {
        this.ti_orderid = ti_orderid;
    }

    public String getTi_sku() {
        return ti_sku;
    }

    public void setTi_sku(String ti_sku) {
        this.ti_sku = ti_sku;
    }

    public String getTi_name() {
        return ti_name;
    }

    public void setTi_name(String ti_name) {
        this.ti_name = ti_name;
    }

    public String getTi_category() {
        return ti_category;
    }

    public void setTi_category(String ti_category) {
        this.ti_category = ti_category;
    }

    public BigDecimal getTi_price() {
        return ti_price;
    }

    public void setTi_price(BigDecimal ti_price) {
        this.ti_price = ti_price;
    }

    public Integer getTi_quantity() {
        return ti_quantity;
    }

    public void setTi_quantity(Integer ti_quantity) {
        this.ti_quantity = ti_quantity;
    }

    public Integer getPp_xoffset_min() {
        return pp_xoffset_min;
    }

    public void setPp_xoffset_min(Integer pp_xoffset_min) {
        this.pp_xoffset_min = pp_xoffset_min;
    }

    public Integer getPp_xoffset_max() {
        return pp_xoffset_max;
    }

    public void setPp_xoffset_max(Integer pp_xoffset_max) {
        this.pp_xoffset_max = pp_xoffset_max;
    }

    public Integer getPp_yoffset_min() {
        return pp_yoffset_min;
    }

    public void setPp_yoffset_min(Integer pp_yoffset_min) {
        this.pp_yoffset_min = pp_yoffset_min;
    }

    public Integer getPp_yoffset_max() {
        return pp_yoffset_max;
    }

    public void setPp_yoffset_max(Integer pp_yoffset_max) {
        this.pp_yoffset_max = pp_yoffset_max;
    }

    public String getUseragent() {
        return useragent;
    }

    public void setUseragent(String useragent) {
        this.useragent = useragent;
    }

    public String getBr_name() {
        return br_name;
    }

    public void setBr_name(String br_name) {
        this.br_name = br_name;
    }

    public String getBr_family() {
        return br_family;
    }

    public void setBr_family(String br_family) {
        this.br_family = br_family;
    }

    public String getBr_version() {
        return br_version;
    }

    public void setBr_version(String br_version) {
        this.br_version = br_version;
    }

    public String getBr_type() {
        return br_type;
    }

    public void setBr_type(String br_type) {
        this.br_type = br_type;
    }

    public String getBr_renderengine() {
        return br_renderengine;
    }

    public void setBr_renderengine(String br_renderengine) {
        this.br_renderengine = br_renderengine;
    }

    public String getBr_lang() {
        return br_lang;
    }

    public void setBr_lang(String br_lang) {
        this.br_lang = br_lang;
    }

    public Boolean isBr_features_pdf() {
        return br_features_pdf;
    }

    public void setBr_features_pdf(Boolean br_features_pdf) {
        this.br_features_pdf = br_features_pdf;
    }

    public Boolean isBr_features_flash() {
        return br_features_flash;
    }

    public void setBr_features_flash(Boolean br_features_flash) {
        this.br_features_flash = br_features_flash;
    }

    public Boolean isBr_features_java() {
        return br_features_java;
    }

    public void setBr_features_java(Boolean br_features_java) {
        this.br_features_java = br_features_java;
    }

    public Boolean isBr_features_director() {
        return br_features_director;
    }

    public void setBr_features_director(Boolean br_features_director) {
        this.br_features_director = br_features_director;
    }

    public Boolean isBr_features_quicktime() {
        return br_features_quicktime;
    }

    public void setBr_features_quicktime(Boolean br_features_quicktime) {
        this.br_features_quicktime = br_features_quicktime;
    }

    public Boolean isBr_features_realplayer() {
        return br_features_realplayer;
    }

    public void setBr_features_realplayer(Boolean br_features_realplayer) {
        this.br_features_realplayer = br_features_realplayer;
    }

    public Boolean isBr_features_windowsmedia() {
        return br_features_windowsmedia;
    }

    public void setBr_features_windowsmedia(Boolean br_features_windowsmedia) {
        this.br_features_windowsmedia = br_features_windowsmedia;
    }

    public Boolean isBr_features_gears() {
        return br_features_gears;
    }

    public void setBr_features_gears(Boolean br_features_gears) {
        this.br_features_gears = br_features_gears;
    }

    public Boolean isBr_features_silverlight() {
        return br_features_silverlight;
    }

    public void setBr_features_silverlight(Boolean br_features_silverlight) {
        this.br_features_silverlight = br_features_silverlight;
    }

    public Boolean isBr_cookies() {
        return br_cookies;
    }

    public void setBr_cookies(Boolean br_cookies) {
        this.br_cookies = br_cookies;
    }

    public String getBr_colordepth() {
        return br_colordepth;
    }

    public void setBr_colordepth(String br_colordepth) {
        this.br_colordepth = br_colordepth;
    }

    public Integer getBr_viewwidth() {
        return br_viewwidth;
    }

    public void setBr_viewwidth(Integer br_viewwidth) {
        this.br_viewwidth = br_viewwidth;
    }

    public Integer getBr_viewheight() {
        return br_viewheight;
    }

    public void setBr_viewheight(Integer br_viewheight) {
        this.br_viewheight = br_viewheight;
    }

    public String getOs_name() {
        return os_name;
    }

    public void setOs_name(String os_name) {
        this.os_name = os_name;
    }

    public String getOs_family() {
        return os_family;
    }

    public void setOs_family(String os_family) {
        this.os_family = os_family;
    }

    public String getOs_manufacturer() {
        return os_manufacturer;
    }

    public void setOs_manufacturer(String os_manufacturer) {
        this.os_manufacturer = os_manufacturer;
    }

    public String getOs_timezone() {
        return os_timezone;
    }

    public void setOs_timezone(String os_timezone) {
        this.os_timezone = os_timezone;
    }

    public String getDvce_type() {
        return dvce_type;
    }

    public void setDvce_type(String dvce_type) {
        this.dvce_type = dvce_type;
    }

    public Boolean isDvce_ismobile() {
        return dvce_ismobile;
    }

    public void setDvce_ismobile(Boolean dvce_ismobile) {
        this.dvce_ismobile = dvce_ismobile;
    }

    public Integer getDvce_screenwidth() {
        return dvce_screenwidth;
    }

    public void setDvce_screenwidth(Integer dvce_screenwidth) {
        this.dvce_screenwidth = dvce_screenwidth;
    }

    public Integer getDvce_screenheight() {
        return dvce_screenheight;
    }

    public void setDvce_screenheight(Integer dvce_screenheight) {
        this.dvce_screenheight = dvce_screenheight;
    }

    public String getDoc_charset() {
        return doc_charset;
    }

    public void setDoc_charset(String doc_charset) {
        this.doc_charset = doc_charset;
    }

    public Integer getDoc_width() {
        return doc_width;
    }

    public void setDoc_width(Integer doc_width) {
        this.doc_width = doc_width;
    }

    public Integer getDoc_height() {
        return doc_height;
    }

    public void setDoc_height(Integer doc_height) {
        this.doc_height = doc_height;
    }

    public String getTr_currency() {
        return tr_currency;
    }

    public void setTr_currency(String tr_currency) {
        this.tr_currency = tr_currency;
    }

    public BigDecimal getTr_total_base() {
        return tr_total_base;
    }

    public void setTr_total_base(BigDecimal tr_total_base) {
        this.tr_total_base = tr_total_base;
    }

    public BigDecimal getTr_tax_base() {
        return tr_tax_base;
    }

    public void setTr_tax_base(BigDecimal tr_tax_base) {
        this.tr_tax_base = tr_tax_base;
    }

    public BigDecimal getTr_shipping_base() {
        return tr_shipping_base;
    }

    public void setTr_shipping_base(BigDecimal tr_shipping_base) {
        this.tr_shipping_base = tr_shipping_base;
    }

    public String getTi_currency() {
        return ti_currency;
    }

    public void setTi_currency(String ti_currency) {
        this.ti_currency = ti_currency;
    }

    public BigDecimal getTi_price_base() {
        return ti_price_base;
    }

    public void setTi_price_base(BigDecimal ti_price_base) {
        this.ti_price_base = ti_price_base;
    }

    public String getBase_currency() {
        return base_currency;
    }

    public void setBase_currency(String base_currency) {
        this.base_currency = base_currency;
    }

    public String getGeo_timezone() {
        return geo_timezone;
    }

    public void setGeo_timezone(String geo_timezone) {
        this.geo_timezone = geo_timezone;
    }

    public String getMkt_clickId() {
        return mkt_clickId;
    }

    public void setMkt_clickId(String mkt_clickId) {
        this.mkt_clickId = mkt_clickId;
    }

    public String getMkt_network() {
        return mkt_network;
    }

    public void setMkt_network(String mkt_network) {
        this.mkt_network = mkt_network;
    }

    public String getEtl_tags() {
        return etl_tags;
    }

    public void setEtl_tags(String etl_tags) {
        this.etl_tags = etl_tags;
    }

    public Date getDvce_sent_tstamp() {
        return dvce_sent_tstamp;
    }

    public void setDvce_sent_tstamp(Date dvce_sent_tstamp) {
        this.dvce_sent_tstamp = dvce_sent_tstamp;
    }

    public String getRefr_domain_userId() {
        return refr_domain_userId;
    }

    public void setRefr_domain_userId(String refr_domain_userId) {
        this.refr_domain_userId = refr_domain_userId;
    }

    public Date getRefr_dvce_tstamp() {
        return refr_dvce_tstamp;
    }

    public void setRefr_dvce_tstamp(Date refr_dvce_tstamp) {
        this.refr_dvce_tstamp = refr_dvce_tstamp;
    }

    public String getDerived_contexts() {
        return derived_contexts;
    }

    public void setDerived_contexts(String derived_contexts) {
        this.derived_contexts = derived_contexts;
    }

    public String getDomain_sessionId() {
        return domain_sessionId;
    }

    public void setDomain_sessionId(String domain_sessionId) {
        this.domain_sessionId = domain_sessionId;
    }

    public Date getDerived_tstamp() {
        return derived_tstamp;
    }

    public void setDerived_tstamp(Date derived_tstamp) {
        this.derived_tstamp = derived_tstamp;
    }

    @Override
    public String toString() {
        return "SnowplowEventModel{" + "app_id=" + app_id + ", platform=" + platform + ", etl_tstamp=" + etl_tstamp + ", collector_tstamp=" + collector_tstamp + ", dvce_tstamp=" + dvce_tstamp + ", event=" + event + ", event_id=" + event_id + ", txn_id=" + txn_id + ", name_tracker=" + name_tracker + ", v_tracker=" + v_tracker + ", v_collector=" + v_collector + ", v_etl=" + v_etl + ", user_id=" + user_id + ", user_ipaddress=" + user_ipaddress + ", user_fingerprint=" + user_fingerprint + ", domain_userid=" + domain_userid + ", domain_sessionidx=" + domain_sessionidx + ", network_userid=" + network_userid + ", geo_country=" + geo_country + ", geo_region=" + geo_region + ", geo_city=" + geo_city + ", geo_zipcode=" + geo_zipcode + ", geo_latitude=" + geo_latitude + ", geo_longitude=" + geo_longitude + ", geo_region_name=" + geo_region_name + ", ip_isp=" + ip_isp + ", ip_organization=" + ip_organization + ", ip_domain=" + ip_domain + ", ip_netspeed=" + ip_netspeed + ", page_url=" + page_url + ", page_title=" + page_title + ", page_referrer=" + page_referrer + ", page_urlscheme=" + page_urlscheme + ", page_urlhost=" + page_urlhost + ", page_urlport=" + page_urlport + ", page_urlpath=" + page_urlpath + ", page_urlquery=" + page_urlquery + ", page_urlfragment=" + page_urlfragment + ", refr_urlscheme=" + refr_urlscheme + ", refr_urlhost=" + refr_urlhost + ", refr_urlport=" + refr_urlport + ", refr_urlpath=" + refr_urlpath + ", refr_urlquery=" + refr_urlquery + ", refr_urlfragment=" + refr_urlfragment + ", refr_medium=" + refr_medium + ", refr_source=" + refr_source + ", refr_term=" + refr_term + ", mkt_medium=" + mkt_medium + ", mkt_source=" + mkt_source + ", mkt_term=" + mkt_term + ", mkt_content=" + mkt_content + ", mkt_campaign=" + mkt_campaign + ", contexts=" + contexts + ", se_category=" + se_category + ", se_action=" + se_action + ", se_label=" + se_label + ", se_property=" + se_property + ", se_value=" + se_value + ", unstruct_event=" + unstruct_event + ", tr_orderid=" + tr_orderid + ", tr_affiliation=" + tr_affiliation + ", tr_total=" + tr_total + ", tr_tax=" + tr_tax + ", tr_shipping=" + tr_shipping + ", tr_city=" + tr_city + ", tr_state=" + tr_state + ", tr_country=" + tr_country + ", ti_orderid=" + ti_orderid + ", ti_sku=" + ti_sku + ", ti_name=" + ti_name + ", ti_category=" + ti_category + ", ti_price=" + ti_price + ", ti_quantity=" + ti_quantity + ", pp_xoffset_min=" + pp_xoffset_min + ", pp_xoffset_max=" + pp_xoffset_max + ", pp_yoffset_min=" + pp_yoffset_min + ", pp_yoffset_max=" + pp_yoffset_max + ", useragent=" + useragent + ", br_name=" + br_name + ", br_family=" + br_family + ", br_version=" + br_version + ", br_type=" + br_type + ", br_renderengine=" + br_renderengine + ", br_lang=" + br_lang + ", br_features_pdf=" + br_features_pdf + ", br_features_flash=" + br_features_flash + ", br_features_java=" + br_features_java + ", br_features_director=" + br_features_director + ", br_features_quicktime=" + br_features_quicktime + ", br_features_realplayer=" + br_features_realplayer + ", br_features_windowsmedia=" + br_features_windowsmedia + ", br_features_gears=" + br_features_gears + ", br_features_silverlight=" + br_features_silverlight + ", br_cookies=" + br_cookies + ", br_colordepth=" + br_colordepth + ", br_viewwidth=" + br_viewwidth + ", br_viewheight=" + br_viewheight + ", os_name=" + os_name + ", os_family=" + os_family + ", os_manufacturer=" + os_manufacturer + ", os_timezone=" + os_timezone + ", dvce_type=" + dvce_type + ", dvce_ismobile=" + dvce_ismobile + ", dvce_screenwidth=" + dvce_screenwidth + ", dvce_screenheight=" + dvce_screenheight + ", doc_charset=" + doc_charset + ", doc_width=" + doc_width + ", doc_height=" + doc_height + ", tr_currency=" + tr_currency + ", tr_total_base=" + tr_total_base + ", tr_tax_base=" + tr_tax_base + ", tr_shipping_base=" + tr_shipping_base + ", ti_currency=" + ti_currency + ", ti_price_base=" + ti_price_base + ", base_currency=" + base_currency + ", geo_timezone=" + geo_timezone + ", mkt_clickId=" + mkt_clickId + ", mkt_network=" + mkt_network + ", etl_tags=" + etl_tags + ", dvce_sent_tstamp=" + dvce_sent_tstamp + ", refr_domain_userId=" + refr_domain_userId + ", refr_dvce_tstamp=" + refr_dvce_tstamp + ", derived_contexts=" + derived_contexts + ", domain_sessionId=" + domain_sessionId + ", derived_tstamp=" + derived_tstamp + '}';
    }
    
    
}