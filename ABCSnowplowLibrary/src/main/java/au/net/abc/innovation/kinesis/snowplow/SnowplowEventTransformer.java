/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation.kinesis.snowplow;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class transforms SnowPlow events into formats used
 * to output to various AWS services
 * 
 * @author Sam Mason (sam.mason@abc.net.au)
 */

public class SnowplowEventTransformer {
        
    private static final String TIMESTAMP_FORMAT            = "yyyy-MM-dd HH:mm:ss";
    private static final String PIPE_DELIMITER              = "|";
    private static final String PIPE_DELIMITER_ENCODED      = ":";
    
    /**
    * Transform a SnowPlow event into a delimited string suitable
    * to be used for the Redshift COPY command on a 0.9.6 Snowplow schema:
    * 
    * see: https://github.com/snowplow/snowplow/blob/0.9.6/4-storage/redshift-storage/sql/atomic-def.sql
    * 
    **/
    
    public String toRedshiftDelimitedString(SnowplowEventModel event, char delimiter){
        StringBuilder buff = new StringBuilder();
        
        buff.append(preprocess(event.getApp_id())).append(delimiter);
        buff.append(preprocess(event.getPlatform())).append(delimiter);
        
        buff.append(preprocess(event.getEtl_tstamp())).append(delimiter);
        buff.append(preprocess(event.getCollector_tstamp())).append(delimiter);
        buff.append(preprocess(event.getDvce_tstamp())).append(delimiter);
        
        buff.append(preprocess(event.getEvent())).append(delimiter);
        buff.append(preprocess(event.getEvent_id())).append(delimiter);
        buff.append(preprocess(event.getTxn_id())).append(delimiter);
        
        buff.append(preprocess(event.getName_tracker())).append(delimiter);
        buff.append(preprocess(event.getV_tracker())).append(delimiter);
        buff.append(preprocess(event.getV_collector())).append(delimiter);
        buff.append(preprocess(event.getV_etl())).append(delimiter);
        
        buff.append(preprocess(event.getUser_id())).append(delimiter);
        buff.append(preprocess(event.getUser_ipaddress())).append(delimiter);
        buff.append(preprocess(event.getUser_fingerprint())).append(delimiter);
        
        buff.append(preprocess(event.getDomain_userid())).append(delimiter);
        buff.append(preprocess(event.getDomain_sessionidx())).append(delimiter);
        buff.append(preprocess(event.getNetwork_userid())).append(delimiter);
        
        buff.append(preprocess(event.getGeo_country())).append(delimiter);
        buff.append(preprocess(event.getGeo_region())).append(delimiter);
        buff.append(preprocess(event.getGeo_city())).append(delimiter);
        buff.append(preprocess(event.getGeo_zipcode())).append(delimiter);
        buff.append(preprocess(event.getGeo_latitude())).append(delimiter);
        buff.append(preprocess(event.getGeo_longitude())).append(delimiter);
        buff.append(preprocess(event.getGeo_region_name())).append(delimiter);
        
        buff.append(preprocess(event.getIp_isp())).append(delimiter);
        buff.append(preprocess(event.getIp_organization())).append(delimiter);
        buff.append(preprocess(event.getIp_domain())).append(delimiter);
        buff.append(preprocess(event.getIp_netspeed())).append(delimiter);
        
        buff.append(preprocess(event.getPage_url())).append(delimiter);
        buff.append(preprocess(event.getPage_title())).append(delimiter);
        buff.append(preprocess(event.getPage_referrer())).append(delimiter);
        
        buff.append(preprocess(event.getPage_urlscheme())).append(delimiter);
        buff.append(preprocess(event.getPage_urlhost())).append(delimiter);
        buff.append(preprocess(event.getPage_urlport())).append(delimiter);
        buff.append(preprocess(event.getPage_urlpath())).append(delimiter);
        buff.append(preprocess(event.getPage_urlquery())).append(delimiter);
        buff.append(preprocess(event.getPage_urlfragment())).append(delimiter);
        
        buff.append(preprocess(event.getRefr_urlscheme())).append(delimiter);
        buff.append(preprocess(event.getRefr_urlhost())).append(delimiter);
        buff.append(preprocess(event.getRefr_urlport())).append(delimiter);
        buff.append(preprocess(event.getRefr_urlpath())).append(delimiter);
        buff.append(preprocess(event.getRefr_urlquery())).append(delimiter);
        buff.append(preprocess(event.getRefr_urlfragment())).append(delimiter);
        
        buff.append(preprocess(event.getRefr_medium())).append(delimiter);
        buff.append(preprocess(event.getRefr_source())).append(delimiter);
        buff.append(preprocess(event.getRefr_term())).append(delimiter);
        
        buff.append(preprocess(event.getMkt_medium())).append(delimiter);
        buff.append(preprocess(event.getMkt_source())).append(delimiter);
        buff.append(preprocess(event.getMkt_term())).append(delimiter);
        buff.append(preprocess(event.getMkt_content())).append(delimiter);
        buff.append(preprocess(event.getMkt_campaign())).append(delimiter);
        
        buff.append(preprocess(event.getContexts())).append(delimiter);
        
        buff.append(preprocess(event.getSe_category())).append(delimiter);
        buff.append(preprocess(event.getSe_action())).append(delimiter);
        buff.append(preprocess(event.getSe_label())).append(delimiter);
        buff.append(preprocess(event.getSe_property())).append(delimiter);
        buff.append(preprocess(event.getSe_value())).append(delimiter);
        
        buff.append(preprocess(event.getUnstruct_event())).append(delimiter);
        
        buff.append(preprocess(event.getTr_orderid())).append(delimiter);
        buff.append(preprocess(event.getTr_affiliation())).append(delimiter);
        buff.append(preprocess(event.getTr_total())).append(delimiter);
        buff.append(preprocess(event.getTr_tax())).append(delimiter);
        buff.append(preprocess(event.getTr_shipping())).append(delimiter);
        buff.append(preprocess(event.getTr_city())).append(delimiter);
        buff.append(preprocess(event.getTr_state())).append(delimiter);
        buff.append(preprocess(event.getTr_country())).append(delimiter);
        
        buff.append(preprocess(event.getTi_orderid())).append(delimiter);
        buff.append(preprocess(event.getTi_sku())).append(delimiter);
        buff.append(preprocess(event.getTi_name())).append(delimiter);
        buff.append(preprocess(event.getTi_category())).append(delimiter);
        buff.append(preprocess(event.getTi_price())).append(delimiter);
        buff.append(preprocess(event.getTi_quantity())).append(delimiter);
        
        buff.append(preprocess(event.getPp_xoffset_min())).append(delimiter);
        buff.append(preprocess(event.getPp_xoffset_max())).append(delimiter);
        buff.append(preprocess(event.getPp_yoffset_min())).append(delimiter);
        buff.append(preprocess(event.getPp_yoffset_max())).append(delimiter);
        
        buff.append(preprocess(event.getUseragent())).append(delimiter);
        
        buff.append(preprocess(event.getBr_name())).append(delimiter);
        buff.append(preprocess(event.getBr_family())).append(delimiter);
        buff.append(preprocess(event.getBr_version())).append(delimiter);
        buff.append(preprocess(event.getBr_type())).append(delimiter);
        buff.append(preprocess(event.getBr_renderengine())).append(delimiter);
        buff.append(preprocess(event.getBr_lang())).append(delimiter);
        
        buff.append(preprocess(event.isBr_features_pdf())).append(delimiter);
        buff.append(preprocess(event.isBr_features_flash())).append(delimiter);
        buff.append(preprocess(event.isBr_features_java())).append(delimiter);
        buff.append(preprocess(event.isBr_features_director())).append(delimiter);
        buff.append(preprocess(event.isBr_features_quicktime())).append(delimiter);
        buff.append(preprocess(event.isBr_features_realplayer())).append(delimiter);
        buff.append(preprocess(event.isBr_features_windowsmedia())).append(delimiter);
        buff.append(preprocess(event.isBr_features_gears())).append(delimiter);
        buff.append(preprocess(event.isBr_features_silverlight())).append(delimiter);
        
        buff.append(preprocess(event.isBr_cookies())).append(delimiter);
        buff.append(preprocess(event.getBr_colordepth())).append(delimiter);
        buff.append(preprocess(event.getBr_viewwidth())).append(delimiter);
        buff.append(preprocess(event.getBr_viewheight())).append(delimiter);
        
        buff.append(preprocess(event.getOs_name())).append(delimiter);
        buff.append(preprocess(event.getOs_family())).append(delimiter);
        buff.append(preprocess(event.getOs_manufacturer())).append(delimiter);
        buff.append(preprocess(event.getOs_timezone())).append(delimiter);
        
        buff.append(preprocess(event.getDvce_type())).append(delimiter);
        buff.append(preprocess(event.isDvce_ismobile())).append(delimiter);
        buff.append(preprocess(event.getDvce_screenwidth())).append(delimiter);
        buff.append(preprocess(event.getDvce_screenheight())).append(delimiter);
        
        buff.append(preprocess(event.getDoc_charset())).append(delimiter);
        buff.append(preprocess(event.getDoc_width())).append(delimiter);
        buff.append(preprocess(event.getDoc_height())).append(delimiter);
                
        buff.append(preprocess(event.getTr_currency())).append(delimiter);
        buff.append(preprocess(event.getTr_total_base())).append(delimiter);
        buff.append(preprocess(event.getTr_tax_base())).append(delimiter);
        buff.append(preprocess(event.getTr_shipping_base())).append(delimiter);
        buff.append(preprocess(event.getTi_currency())).append(delimiter);
        buff.append(preprocess(event.getTi_price_base())).append(delimiter);
        buff.append(preprocess(event.getBase_currency())).append(delimiter);
        
        buff.append(preprocess(event.getGeo_timezone())).append(delimiter);
        
        buff.append(preprocess(event.getMkt_clickId())).append(delimiter);
        buff.append(preprocess(event.getMkt_network())).append(delimiter);
        
        buff.append(preprocess(event.getEtl_tags())).append(delimiter);
        
        buff.append(preprocess(event.getDvce_sent_tstamp())).append(delimiter);
        
        buff.append(preprocess(event.getRefr_domain_userId())).append(delimiter);
        buff.append(preprocess(event.getRefr_dvce_tstamp())).append(delimiter);
        
        buff.append(preprocess(event.getDerived_contexts())).append(delimiter);
        
        buff.append(preprocess(event.getDomain_sessionId())).append(delimiter);
        
        buff.append(preprocess(event.getDerived_tstamp()));
        
        buff.append("\n");
        
        return buff.toString();
    }
    
    ///////////////////////////////////////////////////////////////////////////
    
    private String preprocess(String s){
        if(s == null){
            return "";
        }
        else if(s.contains(PIPE_DELIMITER)){
            return s.replaceAll("\\" + PIPE_DELIMITER, PIPE_DELIMITER_ENCODED);
        }
        else{
            return s;
        }
    }
    
    private String preprocess(Date d){
        if(d == null){
            return "";
        }
        else{
            SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_FORMAT);
            return sdf.format(d);
        }
    }
    
    private String preprocess(Integer i){
        return i == null ? "" : String.valueOf(i.intValue());
    }
    
    private String preprocess(Short s){
        return s == null ? "" : String.valueOf(s.shortValue());
    }
    
    private String preprocess(Double d){
        return d == null ? "" : String.valueOf(d.doubleValue());
    }
    
    private String preprocess(BigDecimal d){
        return d == null ? "" : d.toPlainString();
    }
    
    private String preprocess(Boolean b){
        return b == null ? "" : String.valueOf(b.booleanValue());
    }
}
